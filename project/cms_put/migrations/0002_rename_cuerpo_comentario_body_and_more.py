# Generated by Django 4.2.11 on 2024-06-24 17:12

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('cms_put', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comentario',
            old_name='cuerpo',
            new_name='body',
        ),
        migrations.RenameField(
            model_name='contenido',
            old_name='valor',
            new_name='content',
        ),
        migrations.RemoveField(
            model_name='comentario',
            name='fecha',
        ),
        migrations.RemoveField(
            model_name='comentario',
            name='titulo',
        ),
        migrations.RemoveField(
            model_name='contenido',
            name='clave',
        ),
        migrations.AddField(
            model_name='comentario',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='comentario',
            name='title',
            field=models.CharField(default='Default Title', max_length=128),
        ),
        migrations.AddField(
            model_name='contenido',
            name='name',
            field=models.CharField(default='Default Name', max_length=200),
        ),
    ]
